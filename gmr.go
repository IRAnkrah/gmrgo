// api ref http://multiplayerrobot.com/About/api

package gmrgo

import (
	"bytes"
	"encoding/json"
	"fmt"
	"hash/fnv"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/IRAnkrah/gmrgo/models"
)

var apiRoot string = "http://multiplayerrobot.com/api/Diplomacy"

type queryCache struct {
	gamesAndPlayers models.GMRGamesAndPlayers
	queryHash       uint32
}

var cache queryCache

func hashQuery(s string) uint32 {
	s += time.Now().Format("01-02-2006 15:04:05")

	h := fnv.New32a()
	h.Write([]byte(s))
	return h.Sum32()
}

func AuthernticateUser(authKey string) string {
	resp, err := http.Get(fmt.Sprintf("%s/AuthenticateUser?authKey=%s", apiRoot, authKey))
	if err != nil {
		// TODO: handle
		return ""
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		// TODO: handle
		return ""
	}

	return string(body)
}

func GetGamesAndPlayers(authKey string, playerIds []int64) (models.GMRGamesAndPlayers, error) {

	// create player id string
	var playerIdStr string
	for idx, playerId := range playerIds {
		if idx > 0 {
			playerIdStr += "_"
		}
		playerIdStr += strconv.FormatInt(playerId, 10)
	}

	query := fmt.Sprintf("%s/GetGamesAndPlayers?playerIDText=%s&authKey=%s", apiRoot, playerIdStr, authKey)

	// use hash to not spam api with the same query over and over.
	queryHash := hashQuery(query)
	if cache.queryHash == queryHash {
		return cache.gamesAndPlayers, nil
	}

	resp, err := http.Get(query)
	if err != nil {
		print(err.Error())
		return models.GMRGamesAndPlayers{}, fmt.Errorf("%s failed", query)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		print(err.Error())
		return models.GMRGamesAndPlayers{}, fmt.Errorf("could not read responce body")
	}

	// uncomment if you want to write the content to a file for development purposes
	//os.WriteFile("sample-responce.json", body, fs.ModeIrregular)

	var gamesAndPlayers models.GMRGamesAndPlayers

	err = json.Unmarshal(body, &gamesAndPlayers)
	if err != nil {
		return models.GMRGamesAndPlayers{}, fmt.Errorf("could not parse responce json")
	}

	cache.gamesAndPlayers = gamesAndPlayers
	cache.queryHash = queryHash

	return gamesAndPlayers, nil
}

func GetCurrentTurn(authKey string, gameId int32) (models.GMRCurrentTurn, error) {
	gamesAndPlayers, err := GetGamesAndPlayers(authKey, []int64{})
	if err != nil {
		return models.GMRCurrentTurn{}, err
	}

	for _, game := range gamesAndPlayers.Games {
		if game.GameId == gameId {
			return game.CurrentTurn, nil
		}
	}

	return models.GMRCurrentTurn{}, fmt.Errorf("no matching game found")
}

// this is a test method to be used when you don't want to hit their servers for data.
// func ReadGamesAndPlayers() models.GMRGamesAndPlayers {
// 	data, err := os.ReadFile("sample-responce.json")
// 	if err != nil {
// 		fmt.Printf(err.Error())
// 		return models.GMRGamesAndPlayers{}
// 	}

// 	var gamesAndPlayers models.GMRGamesAndPlayers

// 	err = json.Unmarshal(data, &gamesAndPlayers)
// 	if err != nil {
// 		fmt.Printf(err.Error())
// 		return models.GMRGamesAndPlayers{}
// 	}

// 	return gamesAndPlayers
// }

func GetLatestSaveFileBytes(authKey string, gameId int32) ([]byte, error) {
	query := fmt.Sprintf("%sGetLatestSaveFileBytes?authKey=%s&gameId=%d", apiRoot, authKey, gameId)

	resp, err := http.Get(query)
	if err != nil {
		print(err.Error())
		return nil, err
	}

	saveData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		print(err.Error())
		return nil, err
	}

	return saveData, nil
}

func SubmitTurn(authKey string, turnId int32, saveData []byte) error {
	query := fmt.Sprintf("%sSubmitTurn?authKey=%s&turnId=%d", apiRoot, authKey, turnId)

	body := bytes.NewReader(saveData)
	request, err := http.NewRequest("POST", query, body)
	if err != nil {
		print(err.Error())
		return err
	}

	request.Header.Add("Content-length", strconv.Itoa(len(saveData)))

	resp, err := http.DefaultClient.Do(request)
	if err != nil {
		print(err.Error())
		return err
	}

	submitRespRaw, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		print(err.Error())
		return err
	}

	var submitResp models.GMRSubmitTurnResp
	err = json.Unmarshal(submitRespRaw, &submitResp)
	if err != nil {
		return err
	}

	if submitResp.ResultType == "OK" {
		return nil
	}

	return fmt.Errorf("unable to submit turn returned error: %s", submitResp.ResultType)
}
